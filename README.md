# ConveyorBelt v1.0.0

Transfer items from the storage next to the starting point to the end point's storage using a conveyor belt.
When the storage at the end is full, the belt stops and waits for free capacity.
Use the 'bridge' to go over a wall, into a building or use it to cross another conveyor belt.

If you enjoy this mod, buy me a coffee: https://buymeacoffee.com/parhuzamos 💖

* Readme with details: https://gitlab.com/parhuzamos/conveyorbelt-readme (ideas, limitations, known bugs, changelog, todos)
* Steam Workshop: https://steamcommunity.com/sharedfiles/filedetails/?id=2779981982
* Chat on Discord: https://discord.com/channels/331886561044201473/647074670822162440/1076825677535391854
* Discussion on reddit: https://www.reddit.com/r/Autonauts/comments/11686gn/conveyorbelt_v10_is_out_now/

## Options
* `Belt speed ` - the higher the number, the faster the items are moved.
* `Cheap construction` - true/false, default: `false`. This checkbox allows you to build conveyor belt parts more easily. 
It's turned off by default, so it's more life-like and does not feel like cheating. After toggling the checkbox, 
the __game must be restarted__ (like "Quit to Desktop") because all the mod provided blueprints/buildings/models 
are loaded __before__ the opening screen (and never loaded again, AFAIK as of v140.1). 
* `I'm okay with version` - if a new version of this mod is released, you get a messsage after loading/starting a world. 
After reading the message, use this setting to get rid of that message.

![ConveyorBelt](images/ConveyorBelt-v1.0.0.jpg)

## Ideas

* 'question will we be able to draw from the belt or will we have to wait for it to reach it's end (is it event possible?)'
    [@luzelle#0488](https://discord.com/channels/331886561044201473/647074670822162440/953645969247985725)
* ~~'How insane is the cost and tech level?'~~ 
    [~~@Milo#9630~~](https://discord.com/channels/331886561044201473/331886561044201473/953735789211177024) 
    Solved in [v1.0.0](#100-2023-02-19).
* 'I'm thinking different belts: one made of wood ingredients with speed of ~5, one made of basic metal ~10, one made of metal+advanced materials with speed ~20.'
    [@parhuzamos#2657](https://discord.com/channels/331886561044201473/331886561044201473/953972548960403516)   
    * 'what if using the bot speed upgrade would make it move items faster' 
        [@MEEP of Faith#7277](https://discord.com/channels/331886561044201473/331886561044201473/953813851479105586)
    * 'Or, use the power system to speed things up.  No upgrades, give you base speed.  50 power/10(?)seconds for MK1, 100/15(?)seconds for MK2, 150/30(?)seconds for Mk3' 
        [@Xander Hunt#1208](https://discord.com/channels/331886561044201473/331886561044201473/953815812169404456)
* Transfer of liquids: to be decided: create a bucket at start building, transfer the liquid in the bucket, 
destroy the bucket at stop building. But what happens with the bucket?
* 'can it feed into crafting stations?'
    [@HazirBot](https://www.reddit.com/r/Autonauts/comments/11686gn/comment/j964yco/)

## Limitations

* May crash or slow down your game or eat your memory (but should not). Save often, use the autosave.
* No intersections supported yet: no merge, no split, no crossing (usa the bridge for that).
* Parallel belts next to each other are unsupported yet. Maybe in the future.
* For silos only seeds can be transported
* Liquid materials (where a bucket is required) can not be transported yet, like: milk, water, sand, .... See [Ideas](#ideas).
* Bots (workers) can not be transported probably because of a bug in the game: if a storage is attached to the start building,
any bots - added to the storage - disappears: no bots in the storage, no bot on the belt. (But: if a storage containing bots 
is attached to a start building the bots are transferred properly, but still it's not enabled in this mod.)
* You may need to __move__ a newly created storage next to a start/stop building (to link it and be it's storage) 
because you may not put it there if taken from the "Structure Blueprints" toolbar.
* If a moving item is taken from the belt it may behave unexpectedly.
* In some cases if a belt with moving items is broken (a belt piece is moved/removed), the transferred items must be collected manually.


## Known bugs

* I've seen items were taken out of the start storage even the end storage content type was different. It will be fixed in the future.
* Sometimes items rotate on the belt when travelling without any reason. It will be fixed in the future.


## Changelog

### [1.0.0] - 2023-02-19
* `Cheap construction` checkbox in the options of the mod allows you to build conveyor belt parts more easily. 
Default is `false`, so it's more life-like and does not feel like cheating. This solves [@Milo#9630](https://discord.com/channels/331886561044201473/331886561044201473/953735789211177024).
* Use storages at both start and stop: items are taken from and put into them.
* If the end storage is full the belt stops (just like in almost any game).
* If a path of belts is incomplete, you will see a warning sign. An ok sign can be seen if the path is correct up to that point.
The ok signs are removed if the belt is complete with start and stop.
* At the start belt a sign indicates if the end storage is missing or has the wrong content type.
If no content type is set for the end storage yet, it will be changed to match the storage at the start (but only in this case).


### [0.9] - 2022-03-16
Initial version, just basic functionality with a simple 'bridge' to go 'through' walls.


## Todos
* Create better start/stop/belt/bridge models in Blender
* Fix bridge model to be at the same level with the belt
* Change stock photo/texture for the belt
* Merging
    [@CallMeJeff#2924](https://discord.com/channels/331886561044201473/647074670822162440/953656784889970698)
* Splitting
    [@CallMeJeff#2924](https://discord.com/channels/331886561044201473/647074670822162440/953656784889970698)
* Should not work if in blueprint state 
    [comment](https://steamcommunity.com/sharedfiles/filedetails/comments/2779981982#3189117724398782252) by [@nightinggale](https://steamcommunity.com/profiles/76561198076395653)
* Low height models should not appear "in" the belt 
    [comment](https://steamcommunity.com/sharedfiles/filedetails/comments/2779981982#3189117724398914251) by [@nightinggale](https://steamcommunity.com/profiles/76561198076395653)

<a href="https://www.flaticon.com/authors/freepik" title="Freepik">Fruits, Not equal, Question, Close, Target, Tick icons created by Freepik - Flaticon</a>

@parhuzamos
